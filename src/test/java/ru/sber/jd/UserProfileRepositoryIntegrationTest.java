package ru.sber.jd;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sber.jd.entities.UserProfileEntity;
import ru.sber.jd.repositories.UserProfileRepository;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ContextConfiguration(classes = UserProfileRepository.class)
public class UserProfileRepositoryIntegrationTest {

    @Autowired
    private UserProfileRepository userProfileRepository;


    @Test
    public void whenFindByLogin_thenReturnUserProfileEntity() throws Exception {
        UserProfileEntity userProfileEntity = new UserProfileEntity();
        userProfileEntity.setLogin("TEST_USER").setUserGroup("TEST_GROUP").setEMailAddress("test@test.test").setPassword("test1234");
        userProfileRepository.insertUser(userProfileEntity);

        List<UserProfileEntity> foundList = userProfileRepository.selectByLogin(userProfileEntity.getLogin());
        if (foundList.isEmpty()) {
            assertThat(String.format("В таблице не найден пользователь с логином %s!", userProfileEntity.getLogin()))
                    .isEqualTo(String.format("Должен был найден пользователь с логином %s!", userProfileEntity.getLogin()));
        }

        for (UserProfileEntity f:foundList) {
            assertThat(f.getLogin()).isEqualTo(userProfileEntity.getLogin());
            assertThat(f.getEMailAddress()).isEqualTo(userProfileEntity.getEMailAddress());
            userProfileRepository.deleteUserByLogin(userProfileEntity);
            break;
        }
    }


    @Test
    public void whenFindById_thenReturnUserProfileEntity() throws Exception {
        UserProfileEntity userProfileEntity = new UserProfileEntity();
        userProfileEntity.setLogin("TEST_USER").setUserGroup("TEST_GROUP").setEMailAddress("test@test.test").setPassword("test1234");
        userProfileRepository.insertUser(userProfileEntity);
        boolean checked = false;

        List<UserProfileEntity> foundList = userProfileRepository.selectAll();

        if (foundList.isEmpty()) {
            assertThat("Таблица пустая!").isEqualTo("В таблице есть хотя бы один пользователь!");
        }

        for (UserProfileEntity f:foundList) {
            if (f.getLogin().equals(userProfileEntity.getLogin())
                    && f.getEMailAddress().equals(userProfileEntity.getEMailAddress())
                    && f.getUserGroup().equals(userProfileEntity.getUserGroup())) {
                checked = true;
                Integer testId = f.getId();
                List<UserProfileEntity> foundByIdList = userProfileRepository.selectById(testId);
                if (foundByIdList.isEmpty()) {
                    assertThat(String.format("С id = %s не найден ожидаемый пользователь! Найден %s", testId, f.getLogin()))
                            .isEqualTo(String.format("Должен был быть найден пользователь с логином %s", userProfileEntity.getLogin()));
                }

                for (UserProfileEntity fById:foundByIdList) {
                    assertThat(f.getLogin()).isEqualTo(userProfileEntity.getLogin());
                    assertThat(f.getEMailAddress()).isEqualTo(userProfileEntity.getEMailAddress());
                    assertThat(f.getUserGroup()).isEqualTo(userProfileEntity.getUserGroup());
                    break;
                }
                userProfileRepository.deleteUserByLogin(userProfileEntity);
                break;
            }
        }
        if (!checked) {
            assertThat("В таблице не найден тестовый пользователь!").isEqualTo("А должен был!");
        }

    }


}
