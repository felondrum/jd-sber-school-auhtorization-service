package ru.sber.jd;



import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.sber.jd.dto.SessionStatusDto;
import ru.sber.jd.dto.UserProfileDto;
import ru.sber.jd.services.SessionStatusService;
import ru.sber.jd.services.UserProfileService;

import javax.servlet.http.Cookie;


@SpringBootTest
@AutoConfigureMockMvc


public class WebLayerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserProfileService userProfileService;

    @MockBean
    private SessionStatusService sessionStatusService;

    final String cookieValue = "test_cookie";
    final Cookie testToken = new Cookie("authorization_token", cookieValue);

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        this.mockMvc
                .perform(get("/auth"))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(containsString("Необходимо залогиниться!")));
    }

    @Test
    public void shouldReturnUnauthorizedWithFailedCookie() throws Exception {

        this.mockMvc.perform(get("/auth")
                .cookie(testToken))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(containsString("Необходимо перелогиниться!")));
    }

    @Test
    public void shouldReturnServiceUnavailableAndUserNotFound() throws Exception {

        SessionStatusDto testSessionStatusDto = new SessionStatusDto().setToken("test_token").setId(10);

        when(this.sessionStatusService.findFirstByToken(cookieValue)).thenReturn(testSessionStatusDto);

        this.mockMvc.perform(get("/auth").cookie(testToken))
                .andDo(print())
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string(containsString("Пользователь не найден!")));

    }

    @Test
    public void shouldReturnAccepted() throws Exception {
        SessionStatusDto testSessionStatusDto = new SessionStatusDto().setToken("test_token").setId(10);
        UserProfileDto testUserProfileDto = new UserProfileDto().setId(1).setUserGroup("TEST_USER").setEMailAddress("test@test.test");

        when(this.sessionStatusService.findFirstByToken(cookieValue)).thenReturn(testSessionStatusDto);
        when(this.userProfileService.findFirstById(testSessionStatusDto.getId())).thenReturn(testUserProfileDto);

        this.mockMvc.perform(get("/auth").cookie(testToken))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(content().string(containsString(testUserProfileDto.getEMailAddress())))
                .andExpect(content().string(containsString(testUserProfileDto.getUserGroup())));
    }


}
