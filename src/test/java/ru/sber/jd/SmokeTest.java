package ru.sber.jd;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.sber.jd.controllers.AuthorizationController;
import ru.sber.jd.controllers.RegistrationController;

@SpringBootTest
public class SmokeTest {
    @Autowired
    private AuthorizationController authorizationController;

    @Autowired
    private RegistrationController registrationController;

    @Test
    public void authorizationContextLoad() {
        assertThat(authorizationController).isNotNull();
    }

    @Test
    public void registrationContextLoad() {
        assertThat(registrationController).isNotNull();
    }

}
