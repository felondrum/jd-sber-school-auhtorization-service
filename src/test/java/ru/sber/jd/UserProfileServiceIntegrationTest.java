package ru.sber.jd;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sber.jd.dto.UserProfileDto;
import ru.sber.jd.dto.UserProfileRegistrationDto;
import ru.sber.jd.repositories.UserProfileRepository;
import ru.sber.jd.services.UserProfileService;
import ru.sber.jd.utils.EncodingUtils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@ContextConfiguration(classes = {UserProfileService.class, UserProfileRepository.class, EncodingUtils.class})
public class UserProfileServiceIntegrationTest {

    @Autowired
    private UserProfileService userProfileService;

    @Test
    public void whenFindById_thenReturnListUserProfileDto() throws Exception {
        Integer testId = 1;
        List<UserProfileDto> byId = userProfileService.findById(testId);
        if (byId.isEmpty()) {
            assertThat("Список пользователь пустой!").isEqualTo("Список пользователь не пустой!");
        } else {
            assertThat(byId.get(0).getId()).isEqualTo(testId);
        }
    }

    @Test
    public void whenCheckPasswordByRegexp_thenReturnTrue() {
        boolean testResponse = UserProfileService.checkPasswordRegexp("123");
        assertThat(testResponse).isFalse();
        testResponse = UserProfileService.checkPasswordRegexp("aaasd");
        assertThat(testResponse).isFalse();
        testResponse = UserProfileService.checkPasswordRegexp("somePassword");
        assertThat(testResponse).isFalse();
        testResponse = UserProfileService.checkPasswordRegexp("1234567890");
        assertThat(testResponse).isFalse();
        testResponse = UserProfileService.checkPasswordRegexp("CorrectPassword2020");
        assertThat(testResponse).isTrue();
        }


    @Test
    public void shouldReturnTrueOnPasswordCheck() throws Exception {
        String testPassword = "TESTPASSWORD_1";
        String testLogin = "TEST_USER";
        userProfileService.addUser(new UserProfileRegistrationDto()
                .setLogin(testLogin)
                .setEmail("test@test.test")
                .setPassword(testPassword));

        UserProfileDto firstByLogin = userProfileService.findFirstByLogin(testLogin);

        if (firstByLogin != null) {
            boolean b = userProfileService.checkPassword(firstByLogin.getId(), EncodingUtils.encoder(testPassword));
            assertThat(b).isTrue();
        } else {
            assertThat("Пользователь не найден по логину " + testLogin + "!").isEqualTo("Пользователь найден!");
        }
        userProfileService.removeUserByLogin(testLogin);
    }


}
