package ru.sber.jd.fakes;


import lombok.RequiredArgsConstructor;
import ru.sber.jd.entities.UserProfileEntity;
import ru.sber.jd.repositories.SessionStatusRepository;
import ru.sber.jd.repositories.TransportPasswordStatusRepository;
import ru.sber.jd.repositories.UserProfileRepository;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;


@RequiredArgsConstructor
public class UserProfileTableCreate {
    private final UserProfileRepository userProfileRepository;
    private final SessionStatusRepository sessionStatusRepository;
    private final TransportPasswordStatusRepository transportPasswordStatusRepository;

    public void createTables() throws SQLException {
        userProfileRepository.createTable();
        sessionStatusRepository.createTable();
        transportPasswordStatusRepository.createTable();
    }

    public void addSomeData() throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {

        UserProfileEntity userProfileEntity = new UserProfileEntity();
        userProfileEntity.setLogin("admin").setPassword("admin").setEMailAddress("a@a.ru").setUserGroup("ADMIN");
        userProfileRepository.insertUser(userProfileEntity);
        userProfileEntity = new UserProfileEntity();
        userProfileEntity.setLogin("lazy_user").setPassword("lazy_user").setEMailAddress("b@a.ru").setUserGroup("LAZY_USER");
        userProfileRepository.insertUser(userProfileEntity);
    }

    public void init() throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        createTables();
        addSomeData();
    }

}
