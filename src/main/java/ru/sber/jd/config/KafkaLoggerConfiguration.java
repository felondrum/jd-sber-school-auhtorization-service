package ru.sber.jd.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.sber.jd.ConfigLoggerToKafkaService;
import ru.sber.jd.KafkaAppenderBuilder;
import ru.sber.jd.LoggerComponentCustomBuilder;
import ru.sber.jd.LoggerToKafka;
import ru.sber.jd.controllers.AuthorizationController;
import ru.sber.jd.controllers.RegistrationController;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class KafkaLoggerConfiguration {
    @Bean
    public ConfigLoggerToKafkaService executeConfigLog() {

        List<LoggerComponentCustomBuilder> loggerComponentCustomBuilderList = new ArrayList<>();
        loggerComponentCustomBuilderList.add(new LoggerComponentCustomBuilder("ru.sber.jd"));
        List<KafkaAppenderBuilder> kafkaAppenderBuilderList = new ArrayList<>();
        KafkaAppenderBuilder kafkaAppenderBuilder = new KafkaAppenderBuilder();
        //kafkaAppenderBuilder.setPort("37.46.131.116:9092");

        kafkaAppenderBuilder.setPort("localhost:9092");
        kafkaAppenderBuilderList.add(kafkaAppenderBuilder);
        ConfigLoggerToKafkaService configLog = new ConfigLoggerToKafkaService();
        configLog.setLoggersList(loggerComponentCustomBuilderList);
        configLog.setKafkaAppenderList(kafkaAppenderBuilderList);
        configLog.executeConfigForKafka();
        return configLog;
    }

    @Bean
    public LoggerToKafka loggerToKafkaRegistrationController() {
        LoggerToKafka loggerToKafka = new LoggerToKafka();
        loggerToKafka.setProject("Registration Application");
        loggerToKafka.setAuthor("Anton Filippov");
        loggerToKafka.setaClass(RegistrationController.class);
        return loggerToKafka;
    }


    @Bean
    public LoggerToKafka loggerToKafkaAuthController() {
        LoggerToKafka loggerToKafka = new LoggerToKafka();
        loggerToKafka.setProject("Authorization Application");
        loggerToKafka.setAuthor("Anton Filippov");
        loggerToKafka.setaClass(AuthorizationController.class);
        return loggerToKafka;
    }



}
