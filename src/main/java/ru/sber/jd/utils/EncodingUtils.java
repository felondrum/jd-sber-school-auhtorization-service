package ru.sber.jd.utils;


import ru.sber.jd.services.UserProfileService;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;

//SHA-512
public class EncodingUtils {
    public static String encoder(String stringToEncode) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String saltText = "trytobrakethepass";
        byte[] salt = new byte[17];
        int i = 0;
        for (char a:saltText.toCharArray()) {
            salt[i] = (byte) a;
            i++;
        }
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        byte[] hashedPassword = md.digest(stringToEncode.getBytes(StandardCharsets.UTF_8));
        StringBuilder stringBuilder = new StringBuilder();
        for (byte h:hashedPassword) {
            stringBuilder.append((char) h);
        }
        return stringBuilder.toString();
    }

    public static String tokenGenerator() {
        SecureRandom secureRandom = new SecureRandom();
        Base64.Encoder base64Encoder = Base64.getUrlEncoder();
        byte[] randomBytes = new byte[32];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }

    public static String transportPasswordGenerator() {
        StringBuilder generatedPassword = new StringBuilder();
        String numbers = "0123456789";
        String letters = "abcdefghijklmnopqrstuvdxyz";
        String bigLetters = letters.toUpperCase();
        String inWork = null;
        while (!UserProfileService.checkPasswordRegexp(generatedPassword.toString())) {
            switch (Math.abs(new Random().nextInt() % 3)) {
                case 0:
                    inWork = numbers;
                    break;
                case 1:
                    inWork = letters;
                    break;
                case 2:
                    inWork = bigLetters;
                    break;
            }
            generatedPassword.append(inWork.charAt(Math.abs(new Random().nextInt() % inWork.length())));
        }
        return generatedPassword.toString();
    }

}
