package ru.sber.jd.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtils {
    public static Connection createConnection() {
        try {
            return DriverManager.getConnection("jdbc:h2:~/auth;MODE=PostgreSQL", "sa", "");
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
