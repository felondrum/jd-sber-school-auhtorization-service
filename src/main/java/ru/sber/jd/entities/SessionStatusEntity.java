package ru.sber.jd.entities;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SessionStatusEntity {
    private Integer id;
    private String token;
}
