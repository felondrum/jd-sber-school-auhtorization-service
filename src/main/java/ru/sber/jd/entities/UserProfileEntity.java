package ru.sber.jd.entities;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserProfileEntity {
    private Integer id;
    private String login;
    private String password;
    private String eMailAddress;
    private String userGroup;
}
