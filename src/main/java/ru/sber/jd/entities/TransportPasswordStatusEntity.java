package ru.sber.jd.entities;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TransportPasswordStatusEntity {
    private Integer id;
    private Integer actual;
}
