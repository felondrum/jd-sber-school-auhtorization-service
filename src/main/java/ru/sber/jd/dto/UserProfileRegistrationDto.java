package ru.sber.jd.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserProfileRegistrationDto {
    private String login;
    private String password;
    private String email;
}
