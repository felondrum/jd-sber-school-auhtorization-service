package ru.sber.jd.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserProfileChangeEmailDto {
    private String login;
    private String email;
}
