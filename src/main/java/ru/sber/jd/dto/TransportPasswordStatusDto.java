package ru.sber.jd.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TransportPasswordStatusDto {
    private Integer id;
    private Integer actual;
}
