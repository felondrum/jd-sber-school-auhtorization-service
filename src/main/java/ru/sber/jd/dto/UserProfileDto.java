package ru.sber.jd.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserProfileDto {
    private Integer id;
    private String eMailAddress;
    private String userGroup;
}
