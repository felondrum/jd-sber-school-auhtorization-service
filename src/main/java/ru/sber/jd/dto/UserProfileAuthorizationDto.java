package ru.sber.jd.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserProfileAuthorizationDto {
    private String login;
    private String password;
}
