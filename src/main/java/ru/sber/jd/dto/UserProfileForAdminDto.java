package ru.sber.jd.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserProfileForAdminDto {
    private String login;
    private String userGroup;
    private String email;
    private String password;
}
