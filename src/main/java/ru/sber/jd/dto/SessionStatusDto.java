package ru.sber.jd.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SessionStatusDto {
    private Integer id;
    private String token;
}
