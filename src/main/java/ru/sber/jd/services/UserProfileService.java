package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.*;
import ru.sber.jd.entities.UserProfileEntity;
import ru.sber.jd.exceptions.*;
import ru.sber.jd.repositories.UserProfileRepository;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserProfileService {

    private final UserProfileRepository profileRepository;


    public List<UserProfileDto> findAll() throws SQLException {
        return profileRepository.selectAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<UserProfileForAdminDto> findAllForAdmin() throws SQLException {
        return profileRepository.selectAll().stream().map(this::mapToDtoAdmin).collect(Collectors.toList());
    }

    public List<UserProfileForForeignDto> findAllForForeign(Integer id) throws SQLException {
        return profileRepository.selectById(id).stream().map(this::mapToDtoForeign).collect(Collectors.toList());
    }

    public List<UserProfileDto> findById(Integer id) throws SQLException {
        return profileRepository.selectById(id).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public UserProfileDto findFirstById(Integer id) throws SQLException {
        try {
            return findById(id).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public UserProfileForForeignDto findFirstByIdForeign(Integer id) throws SQLException {
        try {
            return findAllForForeign(id).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<UserProfileDto> findByLogin(String login) throws SQLException {
        return profileRepository.selectByLogin(login).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public UserProfileDto findFirstByLogin(String login) {
        try {
            return findByLogin(login).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<UserProfileDto> findByEmail(String email) throws SQLException {
        return profileRepository.selectByEmail(email).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public UserProfileDto findFirstByEmail(String email) {
        try{
            return findByEmail(email).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<UserProfileForAdminDto> findByGroup(String group) throws SQLException {
        return profileRepository.selectByGroup(group).stream().map(this::mapToDtoAdmin).collect(Collectors.toList());
    }

    public boolean checkPassword(Integer id, String encodedPass) throws SQLException {
        List<UserProfileEntity> userProfileEntityList = profileRepository.selectById(id);
        if (userProfileEntityList.get(0).getPassword().equals(encodedPass)) {
            return true;
        } else {
            return false;
        }
    }


    public boolean addUser(UserProfileRegistrationDto userProfileRegistrationDto) {
        UserProfileEntity userProfileEntity = new UserProfileEntity()
                .setLogin(userProfileRegistrationDto.getLogin())
                .setEMailAddress(userProfileRegistrationDto.getEmail())
                .setPassword(userProfileRegistrationDto.getPassword())
                .setUserGroup("USER");
        try {
            profileRepository.insertUser(userProfileEntity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeUserByLogin(String login) {
        UserProfileEntity userProfileEntity = new UserProfileEntity().setLogin(login);
        try {
            profileRepository.deleteUserByLogin(userProfileEntity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean addUsers(List<UserProfileDto> userProfileDtoList) throws SQLException {
        List<UserProfileEntity> userProfileEntityList = new ArrayList<>();
        for (UserProfileDto dto:userProfileDtoList) {
            userProfileEntityList.add(mapToEntity(dto));
        }
        try {
            profileRepository.insertUsers(userProfileEntityList);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean updateProfilePassword(String password, Integer id) {
        try {
            profileRepository.updateUserPassword(id, password);
            return true;
        } catch (Exception e) {
            return false;
        }
    }



    public boolean updateProfileEmail(String email, Integer id) {
        try {
            profileRepository.updateUserEmailAddress(id, email);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateProfileGroup(String group, Integer id) {
        try {
            profileRepository.updateUserGroup(id, group);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean makeRegistration(UserProfileRegistrationDto dto) throws Exception {
        try {
            UserProfileDto byLogin = findFirstByLogin(dto.getLogin());
            if (byLogin != null) throw new BadLoginException("Логин занят!");
            if (!checkEmailRegexp(dto.getEmail())) throw new BadEmailAddressException("Почта введена неверно!");
            UserProfileDto byEmail = findFirstByEmail(dto.getEmail());
            if (byEmail != null) throw new BadEmailAddressException("Почта занята!");
            if (!checkPasswordRegexp(dto.getPassword())) throw new BadPasswordException("Пароль не соответствует требованиям!");
            UserProfileEntity userProfileEntity = new UserProfileEntity();
            userProfileEntity
                    .setLogin(dto.getLogin())
                    .setPassword(dto.getPassword())
                    .setEMailAddress(dto.getEmail())
                    .setUserGroup("USER");
            profileRepository.insertUser(userProfileEntity);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }



    public static boolean checkEmailRegexp(String email) {
        String emailRegexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        return email.matches(emailRegexp);
    }

    //Minimum eight characters, at least one letter and one number
    public static boolean checkPasswordRegexp(String password) {
        String passwordRegexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";
        return password.matches(passwordRegexp);
    }




    private UserProfileEntity mapToEntity(UserProfileDto dto) throws SQLException {
        try {
            List<UserProfileEntity> userProfileEntities = profileRepository.selectById(dto.getId());
            UserProfileEntity userProfileEntity = userProfileEntities.get(0);
            if (userProfileEntity.getId() == null) throw new NoDataFoundException(String.format("Сотрудник с id = %s не найден.", dto.getId()));
            return userProfileEntity;
        } catch (Exception e) {
            throw e;
        }
    }




    private UserProfileDto mapToDto(UserProfileEntity entity) {
        return new UserProfileDto().setId(entity.getId()).setEMailAddress(entity.getEMailAddress()).setUserGroup(entity.getUserGroup());
    }

    private UserProfileForAdminDto mapToDtoAdmin(UserProfileEntity entity) {
        return new UserProfileForAdminDto().setLogin(entity.getLogin()).setEmail(entity.getEMailAddress()).setUserGroup(entity.getUserGroup()).setPassword(entity.getPassword());
    }

    private UserProfileForForeignDto mapToDtoForeign(UserProfileEntity entity) {
        return new UserProfileForForeignDto().setLogin(entity.getLogin()).setEmail(entity.getEMailAddress()).setUserGroup(entity.getUserGroup());
    }

}
