package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.TransportPasswordStatusDto;
import ru.sber.jd.dto.UserProfileDto;
import ru.sber.jd.entities.TransportPasswordStatusEntity;
import ru.sber.jd.exceptions.DatabaseException;
import ru.sber.jd.exceptions.EmailSendException;
import ru.sber.jd.exceptions.NoDataFoundException;
import ru.sber.jd.repositories.TransportPasswordStatusRepository;
import ru.sber.jd.utils.EncodingUtils;
import ru.sber.jd.utils.MailUtils;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransportPasswordStatusService {
    private final TransportPasswordStatusRepository transportPasswordStatusRepository;
    private final UserProfileService userProfileService;
    private String messageForTempMail = "<h2> Dear user! </h2> " +
            "<p> Your transport password: </p> " +
            "<h4> %s </h4> " +
            "<p> If you did not request for password restoration do not react for this letter! </p>" +
            "<p> Best regards, authorization service administrator! </p>";

    public List<TransportPasswordStatusDto> findAll() throws SQLException {
        return transportPasswordStatusRepository.selectAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<TransportPasswordStatusDto> findById(Integer id) throws SQLException {
        return transportPasswordStatusRepository.selectById(id).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public TransportPasswordStatusDto findFirstById(Integer id) {
        try {
            return findById(id).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<TransportPasswordStatusDto> findByActual(Integer actual) throws SQLException {
        return transportPasswordStatusRepository.selectByActual(actual).stream().map(this::mapToDto).collect(Collectors.toList());
    }


    public boolean addStatus(TransportPasswordStatusDto dto) {
        TransportPasswordStatusEntity entity = mapToEntity(dto);
        try {
            transportPasswordStatusRepository.insertTransportStatus(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean addPositiveStatus(Integer id) {
        try {
            TransportPasswordStatusDto dto = findFirstById(id);
            if (dto == null) {
                TransportPasswordStatusDto newDto = new TransportPasswordStatusDto();
                newDto.setId(id);
                newDto.setActual(1);
                return addStatus(newDto);
            } else if (dto.getActual() == 1) {
                return true;
            } else {
                return updateStatusById(id);
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean addNegativeStatus(Integer id) {
        try {
            TransportPasswordStatusDto dto = findFirstById(id);
            if (dto == null) {
                TransportPasswordStatusDto newDto = new TransportPasswordStatusDto();
                newDto.setId(id);
                newDto.setActual(0);
                return addStatus(newDto);
            } else if (dto.getActual() == 0) {
                return true;
            } else {
                return updateStatusById(id);
            }
        } catch (Exception e) {
            return false;
        }
    }


    public boolean addStatuses(List<TransportPasswordStatusDto> listDto) {
        List<TransportPasswordStatusEntity> listEntities = new ArrayList<>();
        for (TransportPasswordStatusDto dto:listDto) {
            listEntities.add(mapToEntity(dto));
        }
        try {
            transportPasswordStatusRepository.insertTransportStatuses(listEntities);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean updateStatusById(Integer id) {
        try {
            TransportPasswordStatusDto dto = findFirstById(id);
            Integer actual = dto.getActual();
            if (actual == 0) {
                actual = 1;
            } else {
                actual = 0;
            }
            transportPasswordStatusRepository.updateTransportPasswordStatusById(id, actual);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean sendTransportPass(String email) {
        String tPass = EncodingUtils.transportPasswordGenerator();
        try {
            UserProfileDto byEmail = userProfileService.findFirstByEmail(email);
            if (byEmail == null) throw new NoDataFoundException("Указанная почта не зарегистрирована!");
            boolean written = addPositiveStatus(byEmail.getId());
            boolean passChanged = userProfileService.updateProfilePassword(tPass, byEmail.getId());
            if (!written || !passChanged) throw new DatabaseException("Что-то пошло не так!");
            String msg = String.format(messageForTempMail, tPass);
            Session session = MailUtils.createEmailConnection();
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("anton_filippov_jd_school_sber@mail.ru"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Восстановление пароля. JD-SERVICE");
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            message.setContent(multipart);
            Transport.send(message);
            return true;
        } catch (DatabaseException e) {
            throw e;
        } catch (Exception e) {
            throw new EmailSendException("Мы не смогли отправить сообщение!");
        }
    }


    private TransportPasswordStatusEntity mapToEntity(TransportPasswordStatusDto dto) {
        return new TransportPasswordStatusEntity().setId(dto.getId()).setActual(dto.getActual());
    }

    private TransportPasswordStatusDto mapToDto(TransportPasswordStatusEntity entity) {
        return new TransportPasswordStatusDto().setId(entity.getId()).setActual(entity.getActual());
    }

}
