package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.SessionStatusDto;
import ru.sber.jd.dto.TransportPasswordStatusDto;
import ru.sber.jd.dto.UserProfileDto;
import ru.sber.jd.entities.SessionStatusEntity;
import ru.sber.jd.exceptions.AuthorizationFailure;
import ru.sber.jd.exceptions.DatabaseException;
import ru.sber.jd.exceptions.NoDataFoundException;
import ru.sber.jd.exceptions.PasswordShouldBeChanged;
import ru.sber.jd.repositories.SessionStatusRepository;
import ru.sber.jd.utils.EncodingUtils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SessionStatusService {
    private final SessionStatusRepository sessionStatusRepository;
    private final UserProfileService userProfileService;
    private final TransportPasswordStatusService transportPasswordStatusService;

    public List<SessionStatusDto> findAll() throws SQLException {
        return sessionStatusRepository.selectAll().stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public List<SessionStatusDto> findById(Integer id) throws SQLException {
        return sessionStatusRepository.selectById(id).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public SessionStatusDto findFirstById(Integer id) throws SQLException {
        try {
            return findById(id).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<SessionStatusDto> findByToken(String token) throws SQLException {
        return sessionStatusRepository.selectByToken(token).stream().map(this::mapToDto).collect(Collectors.toList());
    }

    public SessionStatusDto findFirstByToken(String token) throws SQLException {
        try {
            return findByToken(token).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean addSession(SessionStatusDto dto) {
        SessionStatusEntity entity = mapToEntity(dto);
        try {
            sessionStatusRepository.insertSession(entity);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removeSession(Integer id) {
        try {
            sessionStatusRepository.deleteSession(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public SessionStatusDto createSession(String login, String password, String token) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        try {
            UserProfileDto dto = userProfileService.findFirstByLogin(login);
            if (dto == null) throw new NoDataFoundException(String.format("Пользователь %s не найден", login));
            boolean isAuth = userProfileService.checkPassword(dto.getId(), EncodingUtils.encoder(password));
            if (!isAuth) throw new AuthorizationFailure("Неверный пароль!");
            TransportPasswordStatusDto actual = transportPasswordStatusService.findFirstById(dto.getId());
            SessionStatusDto sessionStatusDto = new SessionStatusDto().setId(dto.getId()).setToken(token);
            boolean isCleared = removeSession(dto.getId());
            if (!isCleared) throw new DatabaseException("Что-то пошло не так!");
            boolean isCreated = addSession(sessionStatusDto);
            if (!isCreated) throw new DatabaseException("Что-то пошло не так!");
            boolean isTransport = actual != null && actual.getActual() == 1;
            if (isTransport) throw new PasswordShouldBeChanged("Просьба заменить пароль!");
            return sessionStatusDto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void killSessionByLogin(String login) {
        UserProfileDto firstByLogin = userProfileService.findFirstByLogin(login);
        if (firstByLogin == null) throw new NoDataFoundException(String.format("Пользователь %s не найден", login));
        removeSession(firstByLogin.getId());
    }

    private SessionStatusEntity mapToEntity(SessionStatusDto dto) {
        return new SessionStatusEntity().setId(dto.getId()).setToken(dto.getToken());
    }

    private SessionStatusDto mapToDto(SessionStatusEntity entity) {
        return new SessionStatusDto().setId(entity.getId()).setToken(entity.getToken());
    }


}
