package ru.sber.jd.exceptions;

public class PasswordShouldBeChanged extends RuntimeException {
    public PasswordShouldBeChanged(String message) {
        super(message);
    }
}
