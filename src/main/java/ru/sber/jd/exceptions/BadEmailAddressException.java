package ru.sber.jd.exceptions;

public class BadEmailAddressException extends RuntimeException{
    public BadEmailAddressException(String message) {
        super(message);
    }
}
