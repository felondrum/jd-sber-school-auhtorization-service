package ru.sber.jd.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ru.sber.jd.LoggerToKafka;
import ru.sber.jd.dto.*;
import ru.sber.jd.exceptions.*;
import ru.sber.jd.services.SessionStatusService;
import ru.sber.jd.services.TransportPasswordStatusService;
import ru.sber.jd.services.UserProfileService;
import ru.sber.jd.utils.EncodingUtils;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthorizationController {
    private final UserProfileService userProfileService;
    private final SessionStatusService sessionStatusService;
    private final TransportPasswordStatusService transportPasswordStatusService;
    private final RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Resource
    @Qualifier(value = "loggerToKafkaAuthController" )
    private LoggerToKafka log;

    public ResponseEntity createToken(HttpServletResponse httpServletResponse, String login, String password) {
        log.info(String.format("Создание токена для сотрудника %s", login));
        try {
            UserProfileDto firstByLogin = userProfileService.findFirstByLogin(login);
            if (firstByLogin == null) throw new NoDataFoundException("Пользователь не зарегистрирован!");
            String token = EncodingUtils.tokenGenerator();
            Cookie cookie = new Cookie("authorization_token", token);
            cookie.setPath("/");
            cookie.setMaxAge(60);
            httpServletResponse.addCookie(cookie);
            SessionStatusDto sessionStatusDto = new SessionStatusDto();
            sessionStatusDto.setId(firstByLogin.getId()).setToken(token);
            sessionStatusService.createSession(login, password, token);
            return ResponseEntity.ok("Успешная авторизация!");
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }

    }

    public void refreshCookie(Cookie cookie, HttpServletResponse httpServletResponse) {
        cookie.setMaxAge(60);
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
    }


    @GetMapping("/kill_session/{login}")
    public ResponseEntity checkSessionsStatus(@CookieValue(value = "authorization_token", required = false) Cookie cookie,
                                              HttpServletResponse httpServletResponse, @PathVariable String login) {
        log.info(String.format("Запрос на убийство сессии для сотрудника %s", login));
        try {
            ResponseEntity responseEntity = checkAuth(cookie, httpServletResponse);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
                sessionStatusService.killSessionByLogin(login);
                log.info(String.format("Успешный запрос на убийство сессии для сотрудника %s", login));
                return ResponseEntity.ok(String.format("Пользователь %s разлогинился!", login));
            } else {
                throw new DatabaseException("Что-то пошло не так!");
            }
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException | PermissionDeniedException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }


    @PostMapping
    public ResponseEntity authorizeUser(@RequestBody UserProfileAuthorizationDto dto, HttpServletResponse httpServletResponse) {
        log.info(String.format("Авторизация пользователя %s", dto.getLogin()));
        try {
            return createToken(httpServletResponse, dto.getLogin(), dto.getPassword());
        } catch (NoDataFoundException | BadPasswordException e) {
            throw new AuthorizationFailure(e.getMessage());
        } catch (DatabaseException e) {
            throw e;
        }
    }

    @GetMapping
    public ResponseEntity checkAuth(@CookieValue(value = "authorization_token", required = false) Cookie cookie, HttpServletResponse httpServletResponse) {
        log.info(String.format("Проверка аутентификации"));
        if (cookie == null) {
            throw new AuthorizationFailure("Необходимо залогиниться!");
        } else {
            String token = cookie.getValue();
            try {
                SessionStatusDto byToken = sessionStatusService.findFirstByToken(token);
                if (byToken == null) throw new AuthorizationFailure("Необходимо перелогиниться!");
                UserProfileDto userProfileDto = userProfileService.findFirstById(byToken.getId());
                if (userProfileDto == null) throw new NoDataFoundException("Пользователь не найден!");
                refreshCookie(cookie, httpServletResponse);
                log.info(String.format("Данные подтверждены"));
                return new ResponseEntity(userProfileDto, HttpStatus.ACCEPTED);
            } catch (AuthorizationFailure | NoDataFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new DatabaseException("Что-то пошло не так!");
            }
        }
    }

    @GetMapping("/foreign")
    public ResponseEntity checkAuthForeign(@CookieValue(value = "authorization_token", required = false) Cookie cookie, HttpServletResponse httpServletResponse) {

        log.info(String.format("Проверка аутентификации со стороннего сервиса"));
        if (cookie == null) {
            throw new AuthorizationFailure("Необходимо залогиниться!");
        } else {
            String token = cookie.getValue();
            try {
                SessionStatusDto byToken = sessionStatusService.findFirstByToken(token);
                if (byToken == null) throw new AuthorizationFailure("Необходимо залогиниться!");
                UserProfileForForeignDto userProfileDto = userProfileService.findFirstByIdForeign(byToken.getId());
                if (userProfileDto == null) throw new NoDataFoundException("Пользователь не найден!");
                refreshCookie(cookie, httpServletResponse);
                log.info(String.format("Данные для стороннего сервиса подтверждены"));
                return new ResponseEntity(userProfileDto, HttpStatus.ACCEPTED);
            } catch (AuthorizationFailure | NoDataFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new DatabaseException("Что-то пошло не так!");
            }
        }
    }

    @PostMapping("/change_password")
    public ResponseEntity changePassword(@RequestBody UserProfileAuthorizationDto dto
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie, HttpServletResponse httpServletResponse) {
        log.info(String.format("Запрос на изменение пароля для пользователя %s", dto.getLogin()));
        ResponseEntity responseEntity = checkAuth(cookie, httpServletResponse);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            if (!UserProfileService.checkPasswordRegexp(dto.getPassword())) throw new BadPasswordException("Новый пароль не соответствует требованиям!");
            UserProfileDto firstByLogin = (UserProfileDto) responseEntity.getBody();
            userProfileService.updateProfilePassword(dto.getPassword(), firstByLogin.getId());
            log.info(String.format("Успешный запрос на изменение пароля для пользователя %s", dto.getLogin()));
            transportPasswordStatusService.addNegativeStatus(firstByLogin.getId());
        }
        return responseEntity;
    }

    @PostMapping("/change_email")
    public ResponseEntity changeEmail(@RequestBody UserProfileChangeEmailDto dto
            , @CookieValue(value = "authorization_token", required = false) Cookie cookie, HttpServletResponse httpServletResponse) {
        log.info(String.format("Запрос на изменение почты для пользователя %s на %s", dto.getLogin(), dto.getEmail()));
        ResponseEntity responseEntity = checkAuth(cookie, httpServletResponse);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            if (!UserProfileService.checkEmailRegexp(dto.getEmail())) throw new BadEmailAddressException("Новая почта не соответствует требованиям!");
            UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
            userProfileService.updateProfileEmail(dto.getEmail(), userProfileDto.getId());
            log.info(String.format("Успешный запрос на изменение почты для пользователя %s c %s на %s", dto.getLogin(), userProfileDto.getEMailAddress(), dto.getEmail()));
        }
        return responseEntity;
    }

    @GetMapping("/restore_pass/{email}")
    public ResponseEntity restorePassword(@PathVariable String email) {
        log.info(String.format("Запрос на сброс пароля для почты: %s", email));
        if (!UserProfileService.checkEmailRegexp(email)) {
            throw new BadEmailAddressException("Новая почта не соответствует требованиям!");
        }
        try {
            transportPasswordStatusService.sendTransportPass(email);
        } catch (NoDataFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
        log.info(String.format("Успешный запрос на сброс пароля для почты: %s", email));
        return ResponseEntity.ok(String.format("Транспортный пароль отправлен на адрес %s", email));
    }

    @ExceptionHandler(AuthorizationFailure.class)
    public ResponseEntity NotFoundInDb (AuthorizationFailure e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Unauthorized");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(jsonObject.toString());
    }

    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity NotFoundInDb (DatabaseException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Troubles");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(jsonObject.toString());
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity NotFoundInDb (NoDataFoundException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "User not found");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(jsonObject.toString());
    }

    @ExceptionHandler(BadLoginException.class)
    public ResponseEntity LoginAlreadyExists (BadLoginException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Login");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(jsonObject.toString());
    }

    @ExceptionHandler(BadPasswordException.class)
    public ResponseEntity PasswordIsBad (BadPasswordException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Password");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(jsonObject.toString());
    }

    @ExceptionHandler(PasswordShouldBeChanged.class)
    public ResponseEntity PasswordIsBad (PasswordShouldBeChanged e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Change_password");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.GONE).body(jsonObject.toString());
    }

    @ExceptionHandler(PermissionDeniedException.class)
    public ResponseEntity PasswordIsBad (PermissionDeniedException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Permission_denied");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(jsonObject.toString());
    }


}
