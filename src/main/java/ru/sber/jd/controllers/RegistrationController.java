package ru.sber.jd.controllers;


import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.LoggerToKafka;
import ru.sber.jd.dto.UserProfileDto;
import ru.sber.jd.dto.UserProfileRegistrationDto;
import ru.sber.jd.exceptions.*;
import ru.sber.jd.fakes.UserProfileTableCreate;
import ru.sber.jd.repositories.SessionStatusRepository;
import ru.sber.jd.repositories.TransportPasswordStatusRepository;
import ru.sber.jd.repositories.UserProfileRepository;
import ru.sber.jd.services.UserProfileService;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.HashMap;


@RestController
@RequiredArgsConstructor
@RequestMapping("/registration")
public class RegistrationController {
    private final UserProfileService userProfileService;
    private final UserProfileRepository userProfileRepository;
    private final SessionStatusRepository sessionStatusRepository;
    private final TransportPasswordStatusRepository transportPasswordStatusRepository;
    private final AuthorizationController authorizationController;

    @Resource
    @Qualifier(value = "loggerToKafkaRegistrationController" )
    private LoggerToKafka log;

    @GetMapping("/get_all")
    public ResponseEntity findAll(@CookieValue(value = "authorization_token", required = false) Cookie cookie, HttpServletResponse httpServletResponse)  {
        log.info("Запрос на просмотр всех пользователей");
        try {
            ResponseEntity responseEntity = authorizationController.checkAuth(cookie, httpServletResponse);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
                if (!userProfileDto.getUserGroup().equals("ADMIN")) throw new PermissionDeniedException("Недостаточно прав для получения информации!");
                log.info("Успешный запрос на просмотр всех пользователей");
                return ResponseEntity.ok(userProfileService.findAll());
            } else {
                throw new DatabaseException("Что-то пошло не так!");
            }
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException | PermissionDeniedException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }


    @PostMapping
    public ResponseEntity addNewUser(@RequestBody UserProfileRegistrationDto dto, HttpServletResponse httpServletResponse) {
        log.info(String.format("Добавление нового пользователя: %s", dto.getLogin()));
        try {
            boolean wasAdded = userProfileService.makeRegistration(dto);
            if (wasAdded) {
                authorizationController.createToken(httpServletResponse, dto.getLogin(), dto.getPassword());
                log.info(String.format("Успешное добавление нового пользователя: %s", dto.getLogin()));
                return ResponseEntity.ok("Пользователь зарегистрирован!");
            } else {
                throw new RuntimeException();
            }
        } catch (BadEmailAddressException | BadLoginException | BadPasswordException e) {
            throw e;
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged e) {
            throw new AuthorizationFailure(e.getMessage());
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }


    @GetMapping("/init")
    public ResponseEntity init() throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
                UserProfileTableCreate userProfileTableCreate = new UserProfileTableCreate(userProfileRepository, sessionStatusRepository, transportPasswordStatusRepository);
                userProfileTableCreate.createTables();
                userProfileTableCreate.addSomeData();
                return ResponseEntity.ok(userProfileService.findAll());
    }

    @GetMapping("/check")
    public ResponseEntity findAllForAdmin(@CookieValue(value = "authorization_token", required = false) Cookie cookie,
                                          HttpServletResponse httpServletResponse) {
        log.info("Запрос на получение списка пользователей с уровня администратора!");
        try {
            ResponseEntity responseEntity = authorizationController.checkAuth(cookie, httpServletResponse);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
                if (!userProfileDto.getUserGroup().equals("ADMIN")) throw new PermissionDeniedException("Недостаточно прав для получения информации!");
                log.info("Успешный запрос на получение списка пользователей с уровня администратора!");
                return ResponseEntity.ok(userProfileService.findAllForAdmin());
            } else {
                throw new DatabaseException("Что-то пошло не так!");
            }
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException | PermissionDeniedException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }

    @GetMapping("/check_by_group/{group}")
    public ResponseEntity findAllForAdmin(@CookieValue(value = "authorization_token", required = false) Cookie cookie,
                                          HttpServletResponse httpServletResponse, @PathVariable String group) {
        log.info(String.format("Запрос на получение списка пользователей в группе %s", group));
        try {
            ResponseEntity responseEntity = authorizationController.checkAuth(cookie, httpServletResponse);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
                if (!userProfileDto.getUserGroup().equals("ADMIN")) throw new PermissionDeniedException("Недостаточно прав для получения информации!");
                log.info(String.format("Успешный запрос на получение списка пользователей в группе %s", group));
                return ResponseEntity.ok(userProfileService.findByGroup(group));
            } else {
                throw new DatabaseException("Что-то пошло не так!");
            }
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException | PermissionDeniedException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }

    @GetMapping("/update_group/{login}/{group}")
    public ResponseEntity updateUserGroup(@CookieValue(value = "authorization_token", required = false) Cookie cookie,
                                          HttpServletResponse httpServletResponse,@PathVariable String login,@PathVariable String group) {
        log.info(String.format("Запрос на изменение группы у пользователя %s на %s", login, group));
        try {
            ResponseEntity responseEntity = authorizationController.checkAuth(cookie, httpServletResponse);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                UserProfileDto userProfileDto = (UserProfileDto) responseEntity.getBody();
                if (!userProfileDto.getUserGroup().equals("ADMIN")) throw new PermissionDeniedException("Недостаточно прав для получения информации!");
                UserProfileDto targetUser = userProfileService.findFirstByLogin(login);
                if (targetUser == null) throw new NoDataFoundException(String.format("Пользователь %s не найден!", login));
                if (userProfileService.updateProfileGroup(group, targetUser.getId())) {
                    log.info(String.format("Успешный запрос на изменение группы у пользователя %s на %s пользователем %s", login, group));
                    return ResponseEntity.ok(String.format("Группа пользователя %s изменена на %s", login, group));
                } else {
                    throw new DatabaseException("Что-то пошло не так!");
                }
            } else {
                throw new DatabaseException("Что-то пошло не так!");
            }
        } catch (NoDataFoundException | AuthorizationFailure | PasswordShouldBeChanged | DatabaseException | PermissionDeniedException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException("Что-то пошло не так!");
        }
    }

    @ExceptionHandler(AuthorizationFailure.class)
    public ResponseEntity NotFoundInDb (AuthorizationFailure e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Unauthorized");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(jsonObject.toString());
    }

    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity NotFoundInDb (DatabaseException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Troubles");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(jsonObject.toString());
    }

    @ExceptionHandler(BadLoginException.class)
    public ResponseEntity LoginAlreadyExists (BadLoginException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Login");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(jsonObject.toString());
    }

    @ExceptionHandler(BadPasswordException.class)
    public ResponseEntity PasswordIsBad (BadPasswordException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Password");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(jsonObject.toString());
    }

    @ExceptionHandler(BadEmailAddressException.class)
    public ResponseEntity EmailAddressIsBad (BadEmailAddressException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Email");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(jsonObject.toString());
    }

    @ExceptionHandler(PasswordShouldBeChanged.class)
    public ResponseEntity PasswordIsBad (PasswordShouldBeChanged e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Change_password");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.GONE).body(jsonObject.toString());
    }

    @ExceptionHandler(PermissionDeniedException.class)
    public ResponseEntity PasswordIsBad (PermissionDeniedException e) {
        log.error(e.getMessage());
        HashMap<String, String> map = new HashMap<>();
        map.put("tag", "Permission_denied");
        map.put("message", e.getMessage());
        JSONObject jsonObject = new JSONObject(map);
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(jsonObject.toString());
    }

}
