package ru.sber.jd.repositories;

import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.SessionStatusEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Repository
public class SessionStatusRepository {
    private final Integer ID_COLUMN = 1;
    private final Integer TOKEN_COLUMN = 2;

    private final String SESSION_STATUS_TABLE = "session_status";

    private final String SELECT_ALL = String.format("select * from %s", SESSION_STATUS_TABLE);
    private final String SELECT_BY_ID = String.format("select * from %s where id = ?", SESSION_STATUS_TABLE);
    private final String SELECT_BY_TOKEN = String.format("select * from %s where token = ?", SESSION_STATUS_TABLE);

    private final String CREATE_TABLE = String.format("create table %s (id int, token text)", SESSION_STATUS_TABLE);
    private final String DROP_TABLE = String.format("drop table if exists %s", SESSION_STATUS_TABLE);

    private final String INSERT_SESSION = String.format("insert into %s (id, token) values (?, ?)", SESSION_STATUS_TABLE);
    private final String DELETE_SESSION_BY_ID = String.format("delete from %s where id = ?", SESSION_STATUS_TABLE);

    private final Connection connection = DbUtils.createConnection();

    public void createTable() throws SQLException {
        dropTable();
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(CREATE_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    public void dropTable() throws SQLException {
        assert connection != null;
        PreparedStatement dropPreparedStatement = connection.prepareStatement(DROP_TABLE);
        dropPreparedStatement.execute();
        connection.commit();
    }

    public List<SessionStatusEntity> selectAll() throws SQLException {
        return prepareEntityList(executeQuery(SELECT_ALL));
    }

    public List<SessionStatusEntity> selectById(Integer id) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
        preparedStatement.setInt(1, id);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<SessionStatusEntity> selectByToken(String token) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_TOKEN);
        preparedStatement.setString(1, token);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }


    private ResultSet executeQuery(String query) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        return preparedStatement.executeQuery();
    }


    public void insertSession(SessionStatusEntity sessionStatusEntity) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION);
        prepareSessionToInsert(preparedStatement, sessionStatusEntity).executeBatch();
        connection.commit();
    }

    public void insertSessions(List<SessionStatusEntity> sessionStatuses) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION);
        for (SessionStatusEntity sessionStatus:sessionStatuses) {
            preparedStatement = prepareSessionToInsert(preparedStatement, sessionStatus);
        }
        preparedStatement.executeBatch();
        connection.commit();
    }


    public PreparedStatement prepareSessionToInsert(PreparedStatement preparedStatement, SessionStatusEntity sessionStatusEntity) throws SQLException {
        preparedStatement.setInt(ID_COLUMN, sessionStatusEntity.getId());
        preparedStatement.setString(TOKEN_COLUMN, sessionStatusEntity.getToken());
        preparedStatement.addBatch();
        return preparedStatement;
    }

    public void deleteSession(Integer id) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SESSION_BY_ID);
        preparedStatement.setInt(1, id);
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();
    }

    private List<SessionStatusEntity> prepareEntityList(ResultSet resultSet) throws SQLException {
        List<SessionStatusEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new SessionStatusEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setToken(resultSet.getString(TOKEN_COLUMN)));
        }
        return entities;
    }



}
