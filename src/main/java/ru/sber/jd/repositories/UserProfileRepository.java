package ru.sber.jd.repositories;


import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.UserProfileEntity;
import ru.sber.jd.utils.DbUtils;
import ru.sber.jd.utils.EncodingUtils;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserProfileRepository {
    private final Integer ID_COLUMN = 1;
    private final Integer LOGIN_COLUMN = 2;
    private final Integer PASSWORD_COLUMN = 3;
    private final Integer EMAIL_COLUMN = 4;
    private final Integer GROUP_COLUMN = 5;

    private final String USER_PROFILE_TABLE = "user_profile";

    private final String SELECT_ALL = String.format("select * from %s", USER_PROFILE_TABLE);
    private final String SELECT_BY_ID = String.format("select * from %s where id = ?", USER_PROFILE_TABLE);
    private final String SELECT_BY_LOGIN = String.format("select * from %s where login = ?", USER_PROFILE_TABLE);
    private final String SELECT_EMAIL = String.format("select * from %s where upper(email) = upper(?)", USER_PROFILE_TABLE);
    private final String SELECT_ALL_BY_GROUP = String.format("select * from %s where upper(user_group) = upper(?)", USER_PROFILE_TABLE);

    private final String CREATE_TABLE = String.format("create table %s (id int, login text, password text, email text, user_group text)", USER_PROFILE_TABLE);
    private final String DROP_TABLE = String.format("drop table if exists %s", USER_PROFILE_TABLE);

    private final String INSERT_USER = String.format("insert into %s (id, login, password, email, user_group) values (?, ?, ?, ?, ?)", USER_PROFILE_TABLE);

    private final String UPDATE_USER_PASSWORD = String.format("update %s set password = ? where id = ?", USER_PROFILE_TABLE);
    private final String UPDATE_USER_EMAIL = String.format("update %s set email = ? where id = ?", USER_PROFILE_TABLE);
    private final String UPDATE_USER_GROUP = String.format("update %s set user_group = ? where id = ?", USER_PROFILE_TABLE);

    private final String DELETE_USER_BY_LOGIN = String.format("delete from %s where login = ?", USER_PROFILE_TABLE);

    private final String GET_MAX_ID = String.format("select nvl(max(id), 0) + 1 as rslt from %s", USER_PROFILE_TABLE);

    private final Connection connection = DbUtils.createConnection();

    public void createTable() throws SQLException {
        dropTable();
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(CREATE_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    public void dropTable() throws SQLException {
        assert connection != null;
        PreparedStatement dropPreparedStatement = connection.prepareStatement(DROP_TABLE);
        dropPreparedStatement.execute();
        connection.commit();
    }


    public List<UserProfileEntity> selectAll() throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<UserProfileEntity> selectById(Integer id) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
        preparedStatement.setInt(1, id);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<UserProfileEntity> selectByLogin(String login) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LOGIN);
        preparedStatement.setString(1, login);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<UserProfileEntity> selectByEmail(String email) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_EMAIL);
        preparedStatement.setString(1, email);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<UserProfileEntity> selectByGroup(String group) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BY_GROUP);
        preparedStatement.setString(1, group);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);

    }

    public void updateUserPassword(Integer id, String password) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_PASSWORD);
        preparedStatement.setString(1, EncodingUtils.encoder(password));
        preparedStatement.setInt(2, id);
        preparedStatement.addBatch();
        executeQuery(preparedStatement);
    }

    public void updateUserEmailAddress(Integer id, String emailAddress) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_EMAIL);
        preparedStatement.setString(1, emailAddress);
        preparedStatement.setInt(2, id);
        preparedStatement.addBatch();
        executeQuery(preparedStatement);
    }

    public void updateUserGroup(Integer id, String group) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_GROUP);
        preparedStatement.setString(1, group);
        preparedStatement.setInt(2, id);
        preparedStatement.addBatch();
        executeQuery(preparedStatement);
    }

    public void insertUser(UserProfileEntity userEntity) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER);
        preparedStatement = prepareUserToInsert(preparedStatement, userEntity);
        preparedStatement.executeBatch();
        connection.commit();
    }

    public void deleteUserByLogin(UserProfileEntity userEntity) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_BY_LOGIN);
        preparedStatement.setString(1, userEntity.getLogin());
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();
    }

    public void insertUsers(List<UserProfileEntity> userProfiles) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER);
        for (UserProfileEntity userProfile:userProfiles) {
            preparedStatement = prepareUserToInsert(preparedStatement, userProfile);
        }
        preparedStatement.executeBatch();
        connection.commit();
    }

    public PreparedStatement prepareUserToInsert(PreparedStatement preparedStatement, UserProfileEntity userEntity) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        preparedStatement.setInt(ID_COLUMN, getMaxId());
        preparedStatement.setString(LOGIN_COLUMN, userEntity.getLogin());
        preparedStatement.setString(PASSWORD_COLUMN, EncodingUtils.encoder(userEntity.getPassword()));
        preparedStatement.setString(EMAIL_COLUMN, userEntity.getEMailAddress());
        preparedStatement.setString(GROUP_COLUMN, userEntity.getUserGroup());
        preparedStatement.addBatch();
        return preparedStatement;
    }

    private ResultSet executeQuery(String query) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        return preparedStatement.executeQuery();
    }

    private void executeQuery(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.executeBatch();
        assert connection != null;
        connection.commit();
    }

    private Integer getMaxId () throws SQLException {

        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(GET_MAX_ID);
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Integer id = resultSet.getInt(1);
            return id;
        } catch (Exception e) {
            return 1;
        }
    }

    private List<UserProfileEntity> prepareEntityList(ResultSet resultSet) throws SQLException {
        List<UserProfileEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new UserProfileEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setLogin(resultSet.getString(LOGIN_COLUMN))
                    .setPassword(resultSet.getString(PASSWORD_COLUMN))
                    .setEMailAddress(resultSet.getString(EMAIL_COLUMN))
                    .setUserGroup(resultSet.getString(GROUP_COLUMN)));
        }
        return entities;
    }



}
