package ru.sber.jd.repositories;

import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.TransportPasswordStatusEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TransportPasswordStatusRepository {
    private final Integer ID_COLUMN = 1;
    private final Integer ACTUAL_COLUMN = 2;

    private final String TRANSPORT_PASSWORD_STATUS_TABLE = "transport_status";

    private final String SELECT_ALL = String.format("select * from %s", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final String SELECT_BY_ID = String.format("select * from %s where id = ?", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final String SELECT_BY_ACTUAL = String.format("select * from %s where actual = ?", TRANSPORT_PASSWORD_STATUS_TABLE);

    private final String CREATE_TABLE = String.format("create table %s (id int, actual int)", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final String DROP_TABLE = String.format("drop table if exists %s", TRANSPORT_PASSWORD_STATUS_TABLE);


    private final String INSERT_ACTUAL = String.format("insert into %s (id, actual) values (?, ?)", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final String UPDATE_ACTUAL_BY_ID = String.format("update %s set actual = ? where id = ?", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final String DELETE_BY_ID = String.format("delete from %s where id = ?", TRANSPORT_PASSWORD_STATUS_TABLE);
    private final Connection connection = DbUtils.createConnection();

    public void createTable() throws SQLException {
        dropTable();
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(CREATE_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    public void dropTable() throws SQLException {
        assert connection != null;
        PreparedStatement dropPreparedStatement = connection.prepareStatement(DROP_TABLE);
        dropPreparedStatement.execute();
        connection.commit();
    }

    public List<TransportPasswordStatusEntity> selectAll() throws SQLException {
        return prepareEntityList(executeQuery(SELECT_ALL));
    }

    public List<TransportPasswordStatusEntity> selectById(Integer id) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
        preparedStatement.setInt(1, id);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }

    public List<TransportPasswordStatusEntity> selectByActual(Integer actual) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ACTUAL);
        preparedStatement.setInt(1, actual);
        preparedStatement.addBatch();
        ResultSet resultSet = preparedStatement.executeQuery();
        return prepareEntityList(resultSet);
    }


    private ResultSet executeQuery(String query) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        return preparedStatement.executeQuery();
    }

    public void deleteTransportPasswordStatus(Integer id) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID);
        preparedStatement.setInt(1, id);
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();
    }

    public void updateTransportPasswordStatusById(Integer id, Integer actual) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ACTUAL_BY_ID);
        preparedStatement.setInt(1, actual);
        preparedStatement.setInt(2, id);
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
        connection.commit();
    }

    private List<TransportPasswordStatusEntity> prepareEntityList(ResultSet resultSet) throws SQLException {
        List<TransportPasswordStatusEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            entities.add(new TransportPasswordStatusEntity()
                    .setId(resultSet.getInt(ID_COLUMN))
                    .setActual(resultSet.getInt(ACTUAL_COLUMN)));
        }
        return entities;
    }

    public void insertTransportStatus(TransportPasswordStatusEntity transportPasswordStatusEntity) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ACTUAL);
        prepareStatusesToInsert(preparedStatement, transportPasswordStatusEntity).executeBatch();
        connection.commit();
    }

    public void insertTransportStatuses(List<TransportPasswordStatusEntity> transportPasswordStatusEntities) throws SQLException {
        assert connection != null;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ACTUAL);
        for (TransportPasswordStatusEntity transportPasswordStatusEntity:transportPasswordStatusEntities) {
            preparedStatement = prepareStatusesToInsert(preparedStatement, transportPasswordStatusEntity);
        }
        preparedStatement.executeBatch();
        connection.commit();
    }

    public PreparedStatement prepareStatusesToInsert(PreparedStatement preparedStatement, TransportPasswordStatusEntity transportPasswordStatusEntity) throws SQLException {
        preparedStatement.setInt(ID_COLUMN, transportPasswordStatusEntity.getId());
        preparedStatement.setInt(ACTUAL_COLUMN, transportPasswordStatusEntity.getActual());
        preparedStatement.addBatch();
        return preparedStatement;
    }

}
